version: "3.7"

services:

  # To avoid connection issue. The name should be unique for every wordpress instance.
  # Alternatively, you can add the internal IP of the db instance.
  db:
    image: mysql:${MYSQL_VERSION:-5.7}
    volumes:
      # During fresh instance, if you are provided with a backup database you can import it.
      # Create a db folder `mkdir db`, put the backup there.
      # Uncomment the line below and it will execute files with extensions .sh, .sql and .sql.gz.
      # - ./db:/docker-entrypoint-initdb.d
      # - ./db_data:/var/lib/mysql
      - db_data:/var/lib/mysql
      # - /mnt/nfs/funniq_shop_db/funniq_shop_db/mysql:/var/lib/mysql
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        order: start-first
        failure_action: rollback
        delay: 10s
      rollback_config:
        parallelism: 0
        order: stop-first
      placement:
        constraints:
          - 'node.role==worker'
          - 'node.hostname==azure-swarm-worker-52.187.22.141'
    environment:
      MYSQL_ROOT_PASSWORD: ${WP_ROOT_PASSWORD:-root}
      MYSQL_DATABASE: ${WP_DB_NAME:-wordpress}
      MYSQL_USER: ${WP_DB_USER:-wordpress}
      MYSQL_PASSWORD: ${WP_DB_PASSWORD:-wordpress}
    networks:
      - funniq-ingress

  wordpress:
    image: wordpress:${WP_VERSION:-latest}
    volumes:
      # - ./wp-content:/var/www/html/wp-content
      # - wordpress_data:/var/www/html
      - /mnt/nfs/project/funniq/shop/wordpress/html:/var/www/html
    deploy:
      replicas: 1
      update_config:
        parallelism: 1
        order: start-first
        failure_action: rollback
        delay: 10s
      rollback_config:
        parallelism: 0
        order: stop-first
      placement:
        constraints:
          - "node.role==worker"
          - "node.hostname!=aws-swarm-worker-54.151.135.155"        
      labels:
        - "traefik.enable=true"
        - "traefik.tags=public"
        - "traefik.http.services.wordpress.loadbalancer.server.port=80"
        # ---------------------------- unsecured routers ------------------------------- #
        - "traefik.http.routers.wordpress.rule=Host(`shop.funniq.com`)"
        - "traefik.http.routers.wordpress.entrypoints=web"
        - "traefik.http.routers.wordpress.middlewares=fe_https_redirect"
        # ---------------------------- secured router ---------------------------------- #
        - "traefik.http.routers.wordpress-secured.rule=Host(`shop.funniq.com`)"
        - "traefik.http.routers.wordpress-secured.entrypoints=websecure"
        - "traefik.http.routers.wordpress-secured.tls=true"
        - "traefik.http.routers.wordpress-secured.tls.certresolver=myresolver"        
        # ---------------------------- https redirect middleware ----------------------- #
        - "traefik.http.middlewares.fe_https_redirect.redirectscheme.scheme=https"
        - "traefik.http.middlewares.fe_https_redirect.redirectscheme.permanent=true"
        #  ref
        # - "traefik.frontend.entryPoints=https"                
        # - "traefik.frontend.rule=Host:wordpress.docker.localhost" # Modify sub-domain with your project or preferred name.
        # - "traefik.backend=wordpress-localhost" # Define a backend name. Must be unique.    
    environment:
      # To avoid connection issue, the db name should be unique.
      # Alternatively, add the internal IP address instead.
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_NAME: ${WP_DB_NAME:-wordpress}
      WORDPRESS_DB_USER: ${WP_DB_USER:-wordpress}
      WORDPRESS_DB_PASSWORD: ${WP_DB_PASSWORD:-wordpress}
      WORDPRESS_TABLE_PREFIX: ${WORDPRESS_TABLE_PREFIX:-wp_}
    depends_on:
      - db
    networks:
      - funniq-ingress

networks:  
  funniq-ingress:
    external: true

volumes:
  db_data: {}
  wordpress_data: {}